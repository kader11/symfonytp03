<?php
namespace App\Controller;

use App\Entity\EcoleDoctorale;
use App\Entity\These;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ThesisController extends AbstractController
{
    /**
     * @Route("/thesis", name="thesis")
     */
    public function index()
    {
        
        $entityManager = $this->getDoctrine()->getManager();
        $EcoleDoctoralesRepository = $entityManager->getRepository(EcoleDoctorale::class);
        $EcoleDoctorales = $EcoleDoctoralesRepository->findAll();
        
 
        if(empty($EcoleDoctorales)){
            
            $doct1=new EcoleDoctorale();
            $doct1->setNom('EColeDoctoraleParis');
            $doct1->setLien('www.paris13.fr');
            $entityManager->persist($doct1);
            
            
            $these1=new These();
            $these1->setTitre('mathematique et informatique');
            $these1->setPhraseAccroche ('modele linaire et non linaire');
            $these1->setContact('kader.yousfi@ gamil.com');
            $these1->setDesciption("setDescription 1");
            $these1->setEcoleDoctorale($doct1);
            $entityManager->persist($these1);


            
            $these2 = new These();
            $these2->setTitre("geni_logiciel"); 
            $these2->setPhraseAccroche ("modilisation des modeles techenique");
            $these2->setContact("kader@gmail.com");
            $these2->setDesciption("setDescription 2");
            $these2->setEcoleDoctorale($doct1);
            $entityManager->persist($these2);
            $entityManager->flush();


            }
            return $this->render('thesis/index.html.twig',[

                'EcoleDoctorales'=>$EcoleDoctoralesRepository->findAll(),


            ]);
        }
    }



      
      /*  return $this->render('thesis/index.html.twig', [
            
            'controller_name' => 'ThesisController',
           
            'title'=>[
            	['titre'=>'mathematique','contact'=>'kaderyousfi@gmail.fr','Description' =>'Etude de l existence de solutions et de l intregrite de lewy-Stampaccgia'], 

                ['titre'=>'Modélisation liniare','contact'=>'yousfi@gmail.fr', 'Description' => 'Modelisation du transport des porteurs de charge dans les dispositif actifs MESFET'],

            	['titre'=>'GenieLogitciel','contact'=>'kaderyousfi25@gmail.fr','Description' =>'Sciences et techniques educatives'] 

    
            ]
        ]);*/
    

